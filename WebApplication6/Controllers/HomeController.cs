﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class HomeController : Controller
    {
        context c = new context();
        public ActionResult Index()
        {
            var a = c.User.ToList();
            IList<Users1> Users1 = new List<Users1>();

            Users1.Add(new Users1() { id = 1 });
            Users1.Add(new Users1() { id = 2, name = "aa2", username = "u2" });
            Users1.Add(new Users1() { id = 3, name = "aa3", username = "u3" });


            foreach (Users1 std in Users1)
                c.User1.Add(std);
            c.SaveChanges();
            var a1 = c.User1.ToList();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}